Ansible playbooks and modules written for both personal use and employment. All licensed under MIT.

	edgerouter_config		  :	Ansible tasks that enable/disable firewall rules by firewall name and rule regex.
	ovirt_backup_exportstoragedomain  :	Ansible modules/playbook that utilizes oVirt 4.3 REST API to make offline backups of the Export Storage Domain.
	ovirt_export_to_ova		  :	Ansible module/playbook that utilizes oVirt 4.3 REST API to export VM OVAs  (POC. DEPRECATED).
	ovirt_export_to_storagedomain	  :	Ansible module/playbook that utilizes oVirt 4.3 REST API to export to the Export Storage Domain  (POC).
