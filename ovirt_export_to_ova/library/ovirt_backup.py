#!/usr/bin/python

import time
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *

def get_vmid_by_name(engine, vms, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms'.format(engine),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    for i in range(0, len(api_resp_json['vm'])):
        if api_resp_json['vm'][i]['name'] in vms:
            ovirt_obj[api_resp_json['vm'][i]['name']] = api_resp_json['vm'][i]['href'];
    return ovirt_obj

def export_ova_by_vmid(engine, host, hostpath, outfile, vmid, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/export'.format(engine, vmid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action><host><name>{0}</name></host><directory>{1}</directory><filename>{2}</filename></action>'.format(host, hostpath, outfile)).read())
    ovirt_obj = {}
    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def get_status_by_jobid(engine, jobid, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/jobs/{1}'.format(engine, jobid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    ovirt_obj['id'] = api_resp_json['id']
    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['description'] = api_resp_json['description']
    return ovirt_obj

def get_steps_by_jobid(engine, jobid, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/jobs/{1}/steps'.format(engine, jobid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    for i in range(0, len(api_resp_json['step'])):
        ovirt_obj[api_resp_json['step'][i]['id']] = {}
        ovirt_obj[api_resp_json['step'][i]['id']]['step'] = api_resp_json['step'][i]['description']
        ovirt_obj[api_resp_json['step'][i]['id']]['status'] = api_resp_json['step'][i]['status']
        ovirt_obj[api_resp_json['step'][i]['id']]['type'] = api_resp_json['step'][i]['type']
        ovirt_obj[api_resp_json['step'][i]['id']]['job'] = api_resp_json['step'][i]['job']['href']
    return ovirt_obj


def main():

    return_dict = {}
    module_args = dict(
            user=dict(type='str', required=True),
            password=dict(type='str', required=True, no_log=True),
            engine=dict(type='str', required=True),
            host=dict(type='str', required=True),
            hostpath=dict(type='str', required=True),
            vms=dict(type='str', required=True),
    )

    module = AnsibleModule(
            argument_spec=module_args,
            supports_check_mode=True
    )


    vmid_name_dict = get_vmid_by_name(module.params['engine'], module.params['vms'], module.params['user'], module.params['password'])
    for k1 in vmid_name_dict.keys():
        return_dict[k1] = {}
        return_dict[k1]['vm'] = vmid_name_dict[k1]
        return_dict[k1]['status'] = {}
        return_dict[k1]['steps'] = {}
        export_job_dict = export_ova_by_vmid(module.params['engine'], module.params['host'], module.params['hostpath'], '{0}-{1}.ova'.format(k1, time.time()), vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        status_dict = get_status_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
        steps_dict = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

        wait_flag = 1
        while(wait_flag):
            for k2 in steps_dict.keys():
                gen = (x for x in steps_dict.keys() if steps_dict[x]['status'] == 'finished')
                if sorted(steps_dict.keys()) == sorted(gen):
                    return_dict[k1]['status'] = get_status_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    return_dict[k1]['steps'] = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    wait_flag = 0
                else:
                    time.sleep(1)
                    steps_dict = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

    module.exit_json(changed=False, meta=return_dict)

if __name__ == "__main__":
    main()
