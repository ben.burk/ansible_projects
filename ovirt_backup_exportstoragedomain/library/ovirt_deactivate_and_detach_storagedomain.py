#!/usr/bin/python3

import time
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *
from ssl import SSLError

def get_datacenterid_by_name(engine, dc, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/datacenters'.format(engine),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    for i in range(0, len(api_resp_json['data_center'])):
        if api_resp_json['data_center'][i]['name'] == dc:
            ovirt_obj[api_resp_json['data_center'][i]['name']] = api_resp_json['data_center'][i]['href']
    return ovirt_obj

def get_storagedomainid_by_name(engine, dcid, storage_domain, user, password):
    api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/datacenters/{1}/storagedomains'.format(engine, dcid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    for i in range(0, len(api_resp_json['storage_domain'])):
        if api_resp_json['storage_domain'][i]['name'] == storage_domain:
            ovirt_obj[api_resp_json['storage_domain'][i]['name']] = api_resp_json['storage_domain'][i]['href']
    return ovirt_obj

#never used. written before i realized ovirt's api <4.4 doesn't have a function to import existing storage domain
def activate_storagedomain_in_dc(engine, storagedomain_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}{1}/activate'.format(engine, storagedomain_href),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action/>').read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj
    
    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def deactivate_storagedomain_in_dc(engine, storagedomain_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}{1}/deactivate'.format(engine, storagedomain_href),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action/>').read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    return ovirt_obj

def detach_storagedomain_in_dc(engine, host, storagedomain_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/hosts'.format(engine),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
        for i in range(0, len(api_resp_json['host'])):
            if api_resp_json['host'][i]['name'] == host:
                api_resp_json2 = json.loads(open_url('https://{0}{1}?host={2}'.format(engine, '/'+storagedomain_href.split("/")[1]+'/'+storagedomain_href.split("/")[2]+'/'+storagedomain_href.split("/")[5]+'/'+storagedomain_href.split("/")[6], api_resp_json['host'][i]['name']),method='DELETE',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
                ovirt_obj['status'] = api_resp_json2['status']
                ovirt_obj['job'] = api_resp_json2['job']['href']
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
    return ovirt_obj

def get_status_by_storagedomainhref(engine, storagedomain_href, user, password):
    api_resp_json = json.loads(open_url('https://{0}{1}'.format(engine, storagedomain_href),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    ovirt_obj['id'] = api_resp_json['id']
    ovirt_obj['status'] = api_resp_json['status']
    return ovirt_obj

def main():

    return_dict = {}
    module_args = dict(
        user=dict(type='str', required=True, no_log=True),
        password=dict(type='str', required=True, no_log=True),
        engine=dict(type='str', required=True),
        datacenter=dict(type='str', required=True),
        host_name=dict(type='str', required=True),
        storage_domain=dict(type='str', required=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    datacenterid_name_dict = get_datacenterid_by_name(module.params['engine'], module.params['datacenter'], module.params['user'], module.params['password'])
    return_dict['datacenter'] = {}
    return_dict['datacenter'][module.params['datacenter']] = {}
    return_dict['datacenter'][module.params['datacenter']]['id'] = datacenterid_name_dict.get(module.params['datacenter'])
    if datacenterid_name_dict.get(module.params['datacenter']) is None:
        module.fail_json(msg='Could not find datacenter {0}'.format(module.params['datacenter']), **return_dict)
    storagedomainid_name_dict = get_storagedomainid_by_name(module.params['engine'], datacenterid_name_dict[module.params['datacenter']].rsplit('/',1)[1], module.params['storage_domain'], module.params['user'], module.params['password'])
    return_dict['storagedomain'] = {}
    return_dict['storagedomain'][module.params['storage_domain']] = {}
    return_dict['storagedomain'][module.params['storage_domain']]['id'] = storagedomainid_name_dict.get(module.params['storage_domain'])
    if storagedomainid_name_dict.get(module.params['storage_domain']) is None:
        module.fail_json(msg='Could not find storage domain {0}'.format(module.params['storage_domain']), **return_dict)
    deactivate_sd_dict = deactivate_storagedomain_in_dc(module.params['engine'], storagedomainid_name_dict[module.params['storage_domain']], module.params['user'], module.params['password'])
    if deactivate_sd_dict.get('error') is not None:
        module.fail_json(msg="Could not deactivate storagedomain {0}\t:{1}".format(module.params['storage_domain'], deactivate_sd_dict['error']), **return_dict)
    return_dict['storagedomain'][module.params['storage_domain']]['deactivate'] = deactivate_sd_dict['status']
    if deactivate_sd_dict['status'] == "complete":
        sdstatus_dict = get_status_by_storagedomainhref(module.params['engine'], storagedomainid_name_dict[module.params['storage_domain']], module.params['user'], module.params['password'])
        wait_flag = 1
        while(wait_flag):
            if sdstatus_dict['status'] == "maintenance":
                return_dict['storagedomain'][module.params['storage_domain']]['status'] = sdstatus_dict['status']
                wait_flag = 0
            else:
                time.sleep(15)
                sdstatus_dict = get_status_by_storagedomainhref(module.params['engine'], storagedomainid_name_dict[module.params['storage_domain']], module.params['user'], module.params['password'])
        detach_sd_dict = detach_storagedomain_in_dc(module.params['engine'], module.params['host_name'], storagedomainid_name_dict[module.params['storage_domain']], module.params['user'], module.params['password'])
        if detach_sd_dict.get('error') is not None:
            module.fail_json(msg="Could not detach storagedomain {0}\t:{1}".format(module.params['storage_domain'], detach_sd_dict['error']), **return_dict)
        return_dict['storagedomain'][module.params['storage_domain']]['detach'] = detach_sd_dict['status']
        if detach_sd_dict['status'] == "complete":
            module.exit_json(changed=True, meta=return_dict)
        else:
            module.fail_json(msg='Detach storage domain {0} failed!'.format(module.params['storage_domain']), **return_dict)
    else:
        module.fail_json(msg='Deactivate storage domain {0} failed!'.format(module.params['storage_domain']), **return_dict)

if __name__ == "__main__":
    main()
