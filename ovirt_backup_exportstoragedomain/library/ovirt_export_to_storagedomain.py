#!/usr/bin/python3

import sys
if (sys.version_info > (3, 0)):
    from urllib.error import HTTPError
else:
    from urllib2 import HTTPError

import time
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *
from ssl import SSLError

def get_vmid_by_name(engine, vm, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms'.format(engine),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    for i in range(0, len(api_resp_json['vm'])):
        if api_resp_json['vm'][i]['name'] in vm:
            ovirt_obj[api_resp_json['vm'][i]['name']] = api_resp_json['vm'][i]['href']
    return ovirt_obj

def shutdown_vm_by_vmid(engine, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/shutdown'.format(engine, vmid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action/>').read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def start_vm_by_vmid(engine, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/start'.format(engine, vmid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action/>').read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def get_status_by_vmid(engine, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}'.format(engine, vmid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['id'] = api_resp_json['href']
    return ovirt_obj

def get_disks_by_vmid(engine, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/diskattachments'.format(engine, vmid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['disks'] = {}
    for i in range(0, len(api_resp_json['disk_attachment'])):
        ovirt_obj['disks'][api_resp_json['disk_attachment'][i]['disk']['href']] = {}
        ovirt_obj['disks'][api_resp_json['disk_attachment'][i]['disk']['href']]['active'] = api_resp_json['disk_attachment'][i]['active']
        ovirt_obj['disks'][api_resp_json['disk_attachment'][i]['disk']['href']]['bootable'] = api_resp_json['disk_attachment'][i]['bootable']
        ovirt_obj['disks'][api_resp_json['disk_attachment'][i]['disk']['href']]['interface'] = api_resp_json['disk_attachment'][i]['interface']
        ovirt_obj['disks'][api_resp_json['disk_attachment'][i]['disk']['href']]['pass_discard'] = api_resp_json['disk_attachment'][i]['pass_discard']
    return ovirt_obj

def get_template_by_vmid(engine, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}'.format(engine, vmid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
        if api_resp_json.get('original_template') is None:
            return ovirt_obj
        api_resp_json2 = json.loads(open_url('https://{0}/ovirt-engine/api/templates/{1}'.format(engine, api_resp_json['original_template']['href'].rsplit('/',1)[1]),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['template'] = {}
    ovirt_obj['template']['id'] = api_resp_json['original_template']['href']
    ovirt_obj['template']['name'] = api_resp_json2['name']
    return ovirt_obj

def get_diskinfo_by_href(engine, disk_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}{1}'.format(engine, disk_href),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['size'] = api_resp_json['provisioned_size']
    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['name'] = api_resp_json['name']
    return ovirt_obj

def sparsify_disk_by_href(engine, disk_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}{1}/sparsify'.format(engine, disk_href),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action/>').read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    if api_resp_json.get('job') is None:           #avoid KeyError thrown, as if the vm has snapshots, sparsify doesn't happen and there is no jobid. so this might become None when finished
        ovirt_obj['fault'] = True
        ovirt_obj['msg'] = api_resp_json['fault']['reason'] + ': ' + api_resp_json['fault']['detail']
    else:
        ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj 

def detach_disk_from_vm_by_href(engine, vmid, disk_href, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/diskattachments/{2}?detach_only=true'.format(engine, vmid, disk_href.rsplit('/',1)[1]),method='DELETE',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    if api_resp_json.get('status') is None:
        ovirt_obj['fault'] = True
        ovirt_obj['msg'] = api_resp_json['reason'] + ': ' + api_resp_json['detail']
    else:
        ovirt_obj['status'] = api_resp_json['status']
    return ovirt_obj

def attach_disk_to_vm_by_href(engine, vmid, disk_href, bootable, pass_discard, interface, active, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/diskattachments'.format(engine, vmid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<disk_attachment><bootable>{0}</bootable><pass_discard>{1}</pass_discard><interface>{2}</interface><active>{3}</active><disk id="{4}"/></disk_attachment>'.format(bootable, pass_discard, interface, active, disk_href.rsplit('/',1)[1])).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    if api_resp_json.get('id') is None:
        ovirt_obj['fault'] = True
        ovirt_obj['msg'] = api_resp_json['reason'] + ': ' + api_resp_json['detail']
    else:
        ovirt_obj['id'] = api_resp_json['id']
        ovirt_obj['href'] = api_resp_json['href']
    return ovirt_obj

def export_to_storagedomain_by_vmid(engine, storage_domain, exclusive, discard_snapshots, vmid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/vms/{1}/export'.format(engine, vmid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action><storage_domain><name>{0}</name></storage_domain><exclusive>{1}</exclusive><discard_snapshots>{2}</discard_snapshots></action>'.format(storage_domain, exclusive, discard_snapshots)).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def export_to_storagedomain_by_templateid(engine, storage_domain, exclusive, templateid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/templates/{1}/export'.format(engine, templateid),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'},data='<action><storage_domain><name>{0}</name></storage_domain><exclusive>{1}</exclusive></action>'.format(storage_domain, exclusive)).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['job'] = api_resp_json['job']['href']
    return ovirt_obj

def get_status_by_jobid(engine, jobid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/jobs/{1}'.format(engine, jobid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['id'] = api_resp_json['id']
    ovirt_obj['status'] = api_resp_json['status']
    ovirt_obj['description'] = api_resp_json['description']
    return ovirt_obj

def get_steps_by_jobid(engine, jobid, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/jobs/{1}/steps'.format(engine, jobid),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    if api_resp_json.get('step') is None:
        ovirt_obj['fault'] = True
        ovirt_obj['msg'] = api_resp_json
    else:
        for i in range(0, len(api_resp_json['step'])):
            ovirt_obj[api_resp_json['step'][i]['id']] = {}
            ovirt_obj[api_resp_json['step'][i]['id']]['step'] = api_resp_json['step'][i]['description']
            ovirt_obj[api_resp_json['step'][i]['id']]['status'] = api_resp_json['step'][i]['status']
            ovirt_obj[api_resp_json['step'][i]['id']]['type'] = api_resp_json['step'][i]['type']
            ovirt_obj[api_resp_json['step'][i]['id']]['job'] = api_resp_json['step'][i]['job']['href']
    return ovirt_obj

def main():

    return_dict = {}
    module_args = dict(
            user=dict(type='str', required=True, no_log=True),
            password=dict(type='str', required=True, no_log=True),
            engine=dict(type='str', required=True),
            storage_domain=dict(type='str', required=True),
            exclusive=dict(choices=BOOLEANS, required=True),
            discard_snapshots=dict(choices=BOOLEANS, required=True),
            vm=dict(type='str', required=True),
            try_sparsify=dict(choices=BOOLEANS, required=True),
            max_disk_sz=dict(type='int', required=True),
    )

    module = AnsibleModule(
            argument_spec=module_args,
            supports_check_mode=True
    )

    fail = False
    vmstate_fail = ['image_locked', 'migrating', 'not_responding', 'paused', 'powering_down', 'powering_up', 'reboot_in_progress', 'restoring_state', 'saving_state', 'suspended', 'unassigned', 'unknown', 'wait_for_launch']           #as of oVirt 4.3 api, the only other states not in this list are 'up' and 'down'
    diskstate_fail = ['locked', 'illegal']
    vmid_name_dict = get_vmid_by_name(module.params['engine'], module.params['vm'], module.params['user'], module.params['password'])
    if vmid_name_dict.get(module.params['vm']) is None:
        module.fail_json(msg='Could not find vm {0}'.format(module.params['vm']), **return_dict)
    for k1 in vmid_name_dict.keys():
        return_dict[k1] = {}
        return_dict[k1]['vm'] = vmid_name_dict[k1]
        return_dict[k1]['shutdown'] = {}
        return_dict[k1]['start'] = {}
        return_dict[k1]['export'] = {}
        return_dict[k1]['template'] = {}
        return_dict[k1]['shutdown']['status'] = {}
        return_dict[k1]['shutdown']['steps'] = {}
        return_dict[k1]['start']['status'] = {}
        return_dict[k1]['start']['steps'] = {}
        return_dict[k1]['export']['status'] = {}
        return_dict[k1]['export']['steps'] = {}
        return_dict[k1]['template']['status'] = {}
        return_dict[k1]['template']['steps'] = {}
        return_dict[k1]['failure_msgs'] = []
        return_dict[k1]['dont_powerup'] = False

        #shutdown vm
        vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        if vm_state_dict['status'] == 'up':                       #shutdown vm if it is up
            shutdown_job_dict = shutdown_vm_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
            shstatus_dict = get_status_by_jobid(module.params['engine'], shutdown_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            shsteps_dict = get_steps_by_jobid(module.params['engine'], shutdown_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            wait_flag = 1
            while(wait_flag):
                gen1 = (x for x in shsteps_dict.keys() if shsteps_dict[x]['status'] == 'finished' or shsteps_dict[x]['status'] == 'failed')
                if sorted(shsteps_dict.keys()) == sorted(gen1):
                    return_dict[k1]['shutdown']['status'] = get_status_by_jobid(module.params['engine'], shutdown_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    return_dict[k1]['shutdown']['steps'] = get_steps_by_jobid(module.params['engine'], shutdown_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    wait_flag = 0
                else:
                    time.sleep(15)
                    shsteps_dict = get_steps_by_jobid(module.params['engine'], shutdown_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

            if return_dict[k1]['shutdown']['status'].get('status') == 'failed':
                return_dict[k1]['failure_msgs'].append("Shutdown vm {0} failed!:\t{1}".format(k1, json.dumps(return_dict[k1]['shutdown'])))
                module.fail_json(msg="Shutdown vm {0} failed!:\t{1}".format(k1, json.dumps(return_dict[k1]['shutdown'])), **return_dict)
        
            time.sleep(15)
            vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
            wait_flag = 1
            while(wait_flag):
                if vm_state_dict['status'] == 'down':
                    wait_flag = 0
                else:
                    time.sleep(15)
                    vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        elif vm_state_dict['status'] in vmstate_fail:           #if vm is in an unclean state, we bail out
            return_dict[k1]['failure_msgs'].append("Won't shutdown vm {0} while in {1} state!".format(k1, vm_state_dict['status']))
            module.fail_json(msg="Won't shutdown vm {0} while in {1} state!".format(k1, vm_state_dict['status']), **return_dict)
        else:                                                   #if vm was already down, we record this so at the end the vm isn't started and is left down
            return_dict[k1]['dont_powerup'] = True

        #check if vm has disks larger than 'max_disk_sz' . If so, we detach them from their vm. That way the export call below won't process them and they will be effectively excluded from the backup
        diskinfo_dict = get_disks_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        return_dict[k1]['disks'] = {}
        for j1 in diskinfo_dict['disks'].keys():
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]] = {}
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['skip'] = False
            disk_dict = get_diskinfo_by_href(module.params['engine'], j1, module.params['user'], module.params['password'])
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['name'] = disk_dict['name']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['size'] = disk_dict['size']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['status'] = disk_dict['status']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['active'] = diskinfo_dict['disks'][j1]['active']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['bootable'] = diskinfo_dict['disks'][j1]['bootable']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['interface'] = diskinfo_dict['disks'][j1]['interface']
            return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['pass_discard'] = diskinfo_dict['disks'][j1]['pass_discard']
            if int(disk_dict['size']) > int(module.params['max_disk_sz']):
                return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['detach'] = {}
                return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['skip'] = True
                detachdisk_dict = detach_disk_from_vm_by_href(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], j1, module.params['user'], module.params['password'])
                if detachdisk_dict.get('error') is not None:
                    return_dict[k1]['failure_msgs'].append("Can't detach disk {0} from vm {1}\t:{2}".format(disk_dict['name'], k1, detachdisk_dict['error']))
                    module.fail_json(msg="Can't detach disk {0} from vm {1}".format(disk_dict['name'], k1), **return_dict)
                if detachdisk_dict.get('status') is not None:
                    return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['detach']['status'] = detachdisk_dict['status']
                else:
                    return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['detach']['fault'] = detachdisk_dict['fault']
                    return_dict[k1]['disks'][j1.rsplit('/',1)[1]]['detach']['msg'] = detachdisk_dict['msg']
                    return_dict[k1]['failure_msgs'].append("Can't detach disk {0} from vm {1}".format(disk_dict['name'], k1))
                    module.fail_json(msg="Can't detach disk {0} from vm {1}".format(disk_dict['name'], k1), **return_dict)

        #sparsify is attempted. succeed or fail for each disk, the module will continue
        if module.params['try_sparsify'] == '1':
            vmdisk_dict = get_disks_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
            for j2 in vmdisk_dict['disks'].keys():
                return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify'] = {}
                if int(return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['size']) < int(module.params['max_disk_sz']):
                    spsfy_job_dict = sparsify_disk_by_href(module.params['engine'], j2, module.params['user'], module.params['password'])
                    if spsfy_job_dict.get('error') is not None:
                        return_dict[k1]['failure_msgs'].append("Sparsify disk {0} immediately failed!:\t{1}".format(return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['name'], spsfy_job_dict['error']))
                        continue
                    if spsfy_job_dict.get('job') is not None:
                        spsfystatus_dict = get_status_by_jobid(module.params['engine'], spsfy_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                        spsfysteps_dict = get_steps_by_jobid(module.params['engine'], spsfy_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                        wait_flag = 1
                        while(wait_flag):
                            gen3 = (x for x in spsfysteps_dict.keys() if spsfysteps_dict[x]['status'] == 'finished' or spsfysteps_dict[x]['status'] == 'failed')
                            if sorted(spsfysteps_dict.keys()) == sorted(gen3):
                                return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify']['status'] = get_status_by_jobid(module.params['engine'], spsfy_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                                return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify']['steps'] = get_steps_by_jobid(module.params['engine'], spsfy_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                                wait_flag = 0
                            else:
                                time.sleep(15)
                                spsfysteps_dict = get_steps_by_jobid(module.params['engine'], spsfy_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

                        if return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify']['status'].get('status') == 'failed':
                            return_dict[k1]['failure_msgs'].append("Sparsify disk {0} failed!:\t{1}".format(return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['name'], json.dumps(return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify'])))
                    else:
                        return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify']['fault'] = spsfy_job_dict['fault']
                        return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['sparsify']['msg'] = spsfy_job_dict['msg']
                        return_dict[k1]['failure_msgs'].append("Sparsify disk {0} failed!:\t{1}".format(return_dict[k1]['disks'][j2.rsplit('/',1)[1]]['name'], json.dumps(spsfy_job_dict)))

        #see if vm is a derivative of a template. if so export that template, if it exists before the vm
        templateinfo_dict = get_template_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        if templateinfo_dict.get('error') is not None:
            return_dict[k1]['failure_msgs'].append("Could not find original template that was specified from vm metadata for vm {0}:\t{1}".format(k1, templateinfo_dict['error']))
        if templateinfo_dict.get('template') is not None and templateinfo_dict['template'].get('id') is not None and templateinfo_dict['template']['id'].rsplit('/',1)[1] != '00000000-0000-0000-0000-000000000000':
            return_dict[k1]['template']['id'] = templateinfo_dict['template']['id']
            return_dict[k1]['template']['name'] = templateinfo_dict['template']['name']
            export_t_job_dict = export_to_storagedomain_by_templateid(module.params['engine'], module.params['storage_domain'], module.params['exclusive'], templateinfo_dict['template']['id'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            if export_t_job_dict.get('error') is not None:
                fail = True
                return_dict[k1]['failure_msgs'].append("Export template {0} immediately failed! Dependency of vm {1}:\t{2}".format(templateinfo_dict['template']['name'], k1, export_t_job_dict['error']))
            else:
                etstatus_dict = get_status_by_jobid(module.params['engine'], export_t_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                etsteps_dict = get_steps_by_jobid(module.params['engine'], export_t_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                wait_flag = 1
                while(wait_flag):
                    gena = (x for x in etsteps_dict.keys() if etsteps_dict[x]['status'] == 'finished' or etsteps_dict[x]['status'] == 'failed')
                    if sorted(etsteps_dict.keys()) == sorted(gena):
                        return_dict[k1]['template']['status'] = get_status_by_jobid(module.params['engine'], export_t_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                        return_dict[k1]['template']['steps'] = get_steps_by_jobid(module.params['engine'], export_t_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                        wait_flag = 0
                    else:
                        time.sleep(15)
                        etsteps_dict = get_steps_by_jobid(module.params['engine'], export_t_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

                if return_dict[k1]['template']['status'].get('status') == 'failed':
                    fail = True
                    return_dict[k1]['failure_msgs'].append("Export template {0} failed! Dependency of vm {1}:\t{2}".format(templateinfo_dict['template']['name'], k1, json.dumps(return_dict[k1]['template'])))

        time.sleep(15)

        #export vm
        export_job_dict = export_to_storagedomain_by_vmid(module.params['engine'], module.params['storage_domain'], module.params['exclusive'], module.params['discard_snapshots'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        if export_job_dict.get('error') is not None:
            fail = True
            return_dict[k1]['failure_msgs'].append("Export vm {0} immediately failed!:\t{1}".format(k1, export_job_dict['error']))
        else:
            estatus_dict = get_status_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            esteps_dict = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            wait_flag = 1
            while(wait_flag):
                gen4 = (x for x in esteps_dict.keys() if esteps_dict[x]['status'] == 'finished' or esteps_dict[x]['status'] == 'failed')
                if sorted(esteps_dict.keys()) == sorted(gen4):
                    return_dict[k1]['export']['status'] = get_status_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    return_dict[k1]['export']['steps'] = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    wait_flag = 0
                else:
                    time.sleep(15)
                    esteps_dict = get_steps_by_jobid(module.params['engine'], export_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

        if return_dict[k1]['export']['status'].get('status') == 'failed':
            fail = True
            return_dict[k1]['failure_msgs'].append("Export vm {0} failed!:\t{1}".format(k1, json.dumps(return_dict[k1]['export'])))

        time.sleep(15)

        #reattach disks previously detached, if applicable
        for j3 in diskinfo_dict['disks'].keys():
            if return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['skip']:
                return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['attach'] = {}
                attachdisk_dict = attach_disk_to_vm_by_href(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], j3, diskinfo_dict['disks'][j3]['bootable'], diskinfo_dict['disks'][j3]['pass_discard'], diskinfo_dict['disks'][j3]['interface'], diskinfo_dict['disks'][j3]['active'], module.params['user'], module.params['password'])
                if attachdisk_dict.get('error') is not None:
                    return_dict[k1]['failure_msgs'].append("Can't reattach disk {0} to vm {1}\t:{2}".format(return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['name'], k1, attachdisk_dict['error']))
                    module.fail_json(msg="Can't reattach disk {0} to vm {1}".format(return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['name'], k1))
                if attachdisk_dict.get('id') is not None:
                    return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['attach']['status'] = 'complete'
                else:
                    return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['attach']['fault'] = attachdisk_dict['fault']
                    return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['attach']['msg'] = attachdisk_dict['msg']
                    return_dict[k1]['failure_msgs'].append("Can't reattach disk {0} to vm {1}".format(return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['name'], k1))
                    module.fail_json(msg="Can't reattach disk {0} to vm {1}".format(return_dict[k1]['disks'][j3.rsplit('/',1)[1]]['name'], k1), **return_dict)
 
        time.sleep(15)

        #start vm back up
        vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        if vm_state_dict['status'] == 'down':
            if return_dict[k1]['dont_powerup']:                  #finish module early and do not start vm, as vm was powered down at the start so we will leave it that way
                if not fail:
                    module.exit_json(changed=True, meta=return_dict)
                else:
                    module.fail_json(msg='{0} . Check oVirt Engine log for more details!'.format(' '.join(return_dict[k1]['failure_msgs'])), **return_dict)
            start_job_dict = start_vm_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
            if start_job_dict.get('error') is not None:
                return_dict[k1]['failure_msgs'].append("Can't power on vm {0}\t:{1}".format(k1, start_job_dict['error']))
                for a in range(1, 6):       #retry max of 5 times
                    start_job_dict = start_vm_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    if start_job_dict.get('error') is not None:
                        return_dict[k1]['failure_msgs'].append("I{0}: Can't power on vm {1}\t:{2}".format(a+1, k1, start_job_dict['error']))
                        time.sleep(900)       #sleep for 15 minutes before next attempt to power on vm
                    else:
                        break
            if start_job_dict.get('error') is not None:
                module.fail_json(msg='{0} . Check oVirt Engine log for more details!'.format(' '.join(return_dict[k1]['failure_msgs'])), **return_dict)
            ststatus_dict = get_status_by_jobid(module.params['engine'], start_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            ststeps_dict = get_steps_by_jobid(module.params['engine'], start_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
            wait_flag = 1
            while(wait_flag):
                gen5 = (x for x in ststeps_dict.keys() if ststeps_dict[x]['status'] == 'finished' or ststeps_dict[x]['status'] == 'failed')
                if sorted(ststeps_dict.keys()) == sorted(gen5):
                    return_dict[k1]['start']['status'] = get_status_by_jobid(module.params['engine'], start_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    return_dict[k1]['start']['steps'] = get_steps_by_jobid(module.params['engine'], start_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])
                    wait_flag = 0
                else:
                    time.sleep(15)
                    ststeps_dict = get_steps_by_jobid(module.params['engine'], start_job_dict['job'].rsplit('/',1)[1], module.params['user'], module.params['password'])

            if return_dict[k1]['start']['status'].get('status') == 'failed':
                fail = True
                return_dict[k1]['failure_msgs'].append("Start vm {0} failed!:\t{1}".format(k1, json.dumps(return_dict[k1]['start'])))

            time.sleep(15)
            vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
            wait_flag = 1
            while(wait_flag):
                if vm_state_dict['status'] == 'up':
                    wait_flag = 0
                else:
                    time.sleep(15)
                    vm_state_dict = get_status_by_vmid(module.params['engine'], vmid_name_dict[k1].rsplit('/',1)[1], module.params['user'], module.params['password'])
        elif vm_state_dict['status'] in vmstate_fail:
            fail = True
            return_dict[k1]['failure_msgs'].append("Won't start vm {0} while in {1} state!".format(k1, vm_state_dict['status']))

    if not fail:
        module.exit_json(changed=True, meta=return_dict)
    else:
        module.fail_json(msg='{0} . Check oVirt Engine log for more details!'.format(' '.join(return_dict[k1]['failure_msgs'])), **return_dict)

if __name__ == "__main__":
    main()
