#!/usr/bin/python3

import time
from ansible.module_utils.basic import *
from ansible.module_utils.urls import *
from ssl import SSLError

def create_new_storagedomain(engine, host, name, stype, sdtype, spath, user, password):
    ovirt_obj = {}
    try:
        api_resp_json = json.loads(open_url('https://{0}/ovirt-engine/api/storagedomains'.format(engine),method='POST',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Content-type':'application/xml','Accept':'application/json'}, data='<storage_domain><name>{0}</name><type>{1}</type><storage><type>{2}</type><path>{3}</path></storage><host><name>{4}</name></host></storage_domain>'.format(name, sdtype, stype, spath, host)).read())
    except HTTPError as h_err:
        ovirt_obj['error'] = str(h_err)
        return ovirt_obj
    except SSLError as ssl_err:
        ovirt_obj['error'] = str(ssl_err)
        return ovirt_obj

    ovirt_obj['id'] = api_resp_json['href']
    ovirt_obj['status'] = api_resp_json.get('status')         #avoid KeyError thrown, as once the storagedomain becomes available, there is no status. so this becomes None when finished
    return ovirt_obj

def get_status_by_storagedomainhref(engine, storagedomain_href, user, password):
    api_resp_json = json.loads(open_url('https://{0}{1}'.format(engine, storagedomain_href),method='GET',url_username=user,url_password=password,timeout=30,validate_certs=False,headers={'Accept':'application/json'}).read())
    ovirt_obj = {}
    ovirt_obj['id'] = api_resp_json['href']
    ovirt_obj['status'] = api_resp_json.get('status')         #avoid KeyError thrown, as once the storagedomain becomes available, there is no status. so this becomes None when finished
    return ovirt_obj

def main():

    return_dict = {}
    module_args = dict(
        user=dict(type='str', required=True, no_log=True),
        password=dict(type='str', required=True, no_log=True),
        engine=dict(type='str', required=True),
        host_name=dict(type='str', required=True),
        storage_domain=dict(type='str', required=True),
        sdtype=dict(type='str', required=True),
        stype=dict(type='str', required=True),
        spath=dict(type='str', required=True),
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    create_sd_dict = create_new_storagedomain(module.params['engine'], module.params['host_name'], module.params['storage_domain'], module.params['stype'], module.params['sdtype'], module.params['spath'], module.params['user'], module.params['password'])
    if create_sd_dict.get('error') is not None:
        module.fail_json(msg="Can't create new storagedomain {0}\t:{1}".format(module.params['storage_domain'], create_sd_dict['error']), **return_dict)
    return_dict['storagedomain'] = {}
    return_dict['storagedomain']['id'] = create_sd_dict['id']
    return_dict['storagedomain']['status'] = create_sd_dict['status']
    if create_sd_dict['status'] is not None:
        wait_flag = 1
        while(wait_flag):
            if create_sd_dict['status'] is None:
                wait_flag = 0
            else:
                time.sleep(15)
                create_sd_dict = get_status_by_storagedomainhref(module.params['engine'], create_sd_dict['id'], module.params['user'], module.params['password'])
                
    module.exit_json(changed=True, meta=return_dict)

if __name__ == "__main__":
    main()
