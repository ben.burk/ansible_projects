Requires `community.network.edgeos_config` and `community.network.edgeos_command`

Contains task lists for enabling/disabling existing firewall rules
i.e.

---

- name: Enable fw rule
  tasks:
    - import_tasks: tasks/enable_fwrule.yml

- name: Disable fw rule
  tasks:
    - import_tasks: tasks/disable_fwrule.yml
